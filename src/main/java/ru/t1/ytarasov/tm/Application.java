package ru.t1.ytarasov.tm;

import ru.t1.ytarasov.tm.constant.ArgumentConst;
import ru.t1.ytarasov.tm.constant.TerminalConst;

import java.util.Scanner;


public class Application {

    public static void main(String[] args) {
        runArguments(args);
    }

    private static void runArguments(String[] args) {
        if (args == null || args.length == 0) {
            runCommands();
            return;
        }
        final String arg = args[0];
        runArgument(arg);
    }

    private static void runArgument(final String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT:
                displayAbout();
                break;
            case ArgumentConst.VERSION:
                displayVersion();
                break;
            case ArgumentConst.HELP:
                displayHelp();
                break;
            default:
                displayError();
        }
    }

    private static void runCommands() {
        displayWelcome();
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (true) {
            command = scanner.nextLine();
            runCommand(command);
        }
    }

    private static void runCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT:
                displayAbout();
                break;
            case TerminalConst.VERSION:
                displayVersion();
                break;
            case TerminalConst.HELP:
                displayHelp();
                break;
            case TerminalConst.EXIT:
                exitApplication();
            default:
                displayError();
        }
    }

    private static void displayHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s - Display program version.\n", TerminalConst.VERSION, ArgumentConst.VERSION);
        System.out.printf("%s, %s - Display developer info.\n", TerminalConst.ABOUT, ArgumentConst.ABOUT);
        System.out.printf("%s, %s - Display list of terminal commands.\n", TerminalConst.HELP, ArgumentConst.HELP);
        System.out.printf("%s - Exit from application.\n", TerminalConst.EXIT);
    }

    private static void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.1");
    }

    private static void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Yuriy Tarasov");
        System.out.println("ytarasov@t1-consulting.ru");
    }

    private static void displayError() {
        System.out.println("Error. Type help to show availible commands");
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO THE TASK MANAGER **");
    }

    private static void exitApplication() {
        System.exit(0);
    }

}
