package ru.t1.ytarasov.tm.constant;

public class ArgumentConst {

    public final static String VERSION = "-v";

    public final static String HELP = "-h";

    public final static String ABOUT = "-a";

}
